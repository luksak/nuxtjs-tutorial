import Vuex from 'vuex'
import Cookie from 'js-cookie'
import nodeCookie from 'cookie'

const createStore = () => {
  return new Vuex.Store({
    state: {
      loadedPosts: {},
      token: null
    },
    mutations: {
      setPosts(state, posts) {
        state.loadedPosts = { ...state.loadedPosts, ...posts }
      },
      setUserToken(state, token) {
        state.token = token
      },
      clearToken(state) {
        state.token = null
      }
    },
    actions: {
      setPosts(vuexContext, posts) {
        vuexContext.commit('setPosts', posts)
      },
      getPosts(vuexContext, context) {
        return this.$axios.$get(`/posts.json`).then((data) => {
          const posts = {}
          for (const key in data) {
            posts[key] = { ...data[key], id: key }
          }
          vuexContext.commit('setPosts', posts)
        })
      },
      getPost(vuexContext, context) {
        if (vuexContext.getters.loadedPosts[context]) {
          return vuexContext.getters.loadedPosts[context]
        } else {
          return this.$axios.$get(`/posts/${context}.json`).then((data) => {
            const posts = {}
            posts[context] = { ...data, id: context }
            vuexContext.commit('setPosts', posts)
          })
        }
      },
      updatePost(vuexContext, context) {
        return this.$axios
          .$put(
            `/posts/${context.id}.json?auth=${vuexContext.state.token}`,
            context
          )
          .then((data) => {
            const posts = {}
            posts[context.id] = { context }
            vuexContext.commit('setPosts', posts)
          })
      },
      createPost(vuexContext, context) {
        this.$axios
          .$post('/posts.json', {
            ...context,
            updatedDate: new Date()
          })
          .then((data) => {
            const posts = {}
            posts[context] = { ...context, id: data.data.name }
            vuexContext.commit('setPosts', posts)
          })
      },
      authenticateUser(vuexContext, authData) {
        let endpoint = ''
        if (authData.isLogin) {
          endpoint = 'accounts:signInWithPassword'
        } else {
          endpoint = 'accounts:signUp'
        }
        return this.$axios
          .$post(
            `https://identitytoolkit.googleapis.com/v1/${endpoint}?key=${process.env.fireBaseApiKey}`,
            {
              email: authData.email,
              password: authData.password,
              returnSecureToken: true
            }
          )
          .then((result) => {
            const expiresIn = Number.parseInt(result.expiresIn) * 1000
            vuexContext.commit('setUserToken', result.idToken)
            Cookie.set('token', result.idToken)
            Cookie.set('tokenExpirationDate', new Date().getTime() + expiresIn)
          })
      },
      setLogoutTimer(vuexContext, duration) {
        setTimeout(() => {
          vuexContext.dispatch('userLogout')
        }, duration)
      },
      initAuth(vuexContext, req) {
        let token
        let tokenExpiry
        if (req) {
          if (!req.headers.cookie) {
            return
          }
          const cookies = nodeCookie.parse(req.headers.cookie)
          token = cookies.token
          tokenExpiry = cookies.tokenExpirationDate
        } else {
          token = Cookie.get('token')
          tokenExpiry = Cookie.get('tokenExpirationDate')
        }
        if (new Date().getTime() > Number.parseInt(tokenExpiry) || !token) {
          vuexContext.dispatch('userLogout')
        }
        vuexContext.commit('setUserToken', token)
      },
      userLogout(vuexContext) {
        vuexContext.commit('clearToken')
        Cookie.remove('token')
        Cookie.remove('tokenExpirationDate')
      }
    },
    getters: {
      loadedPosts(state) {
        return state.loadedPosts
      },
      isAuthenticated(state) {
        return state.token != null
      }
    }
  })
}

export default createStore
